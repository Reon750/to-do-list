# Utiliza la imagen de Node.js versión 20.7.0 como la imagen base para el proceso de construcción
FROM node:20.7.0 AS builder

# Establece el directorio de trabajo dentro del contenedor como /app
WORKDIR /app

# Copia los archivos package.json y package-lock.json (si existe) al directorio de trabajo en el contenedor
COPY package*.json ./

# Instala las dependencias definidas en package.json
RUN npm i

# Copia todo el contenido de la carpeta actual del host al directorio de trabajo del contenedor
COPY . .

# Ejecuta el script de construcción definido en package.json para crear la versión optimizada del frontend
RUN npm run build

# Utiliza la imagen de Nginx más reciente como la imagen base para servir el frontend
FROM nginx:latest

# Copia el archivo de configuración personalizado de Nginx al directorio de configuración de Nginx en el contenedor
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

# Copia los archivos construidos en la etapa de construcción al directorio predeterminado de Nginx para servir contenido estático
COPY --from=builder /app/dist /usr/share/nginx/html

# Expone el puerto 80 para que el contenedor sirva el contenido en ese puerto
EXPOSE 80

# Define el comando por defecto para ejecutar Nginx en modo no daemon
CMD ["nginx", "-g", "daemon off;"]
